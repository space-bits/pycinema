# Python cinema modelling program

This app was mostly built for fun in an attempt to model a real world cinema.
Features to include but are not limited to:
	-	Site, containing cinemas
	- Cinema, containing screen, seats
	- Seat, offers support for booked status, and for waiters to be called
	- Staff, which includes waiters, who serve the needs of patrons
	- Patrons, who can place orders
	- Orders, responsible for assuring that food and drink items are deliverd
	- Consumables, extended by food and beverage, responsible for pricing

