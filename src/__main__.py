from enum import Enum, auto

from Cinema import Cinema
from Screen import Screen


class Concepts(Enum):
	GOLD_CLASS=auto()
	TRADITIONAL=auto()
	VMAX=auto()
	VPREMIUM=auto()
	VJUNIOR=auto()
	CINEMA_EUROPA=auto()
	DRIVEIN=auto()

def create_site():
	'''Creates a site for the cinemas, consisting of a collection of Cinema
	objects'''
	# define all our needed structures to keep track of the site details
	cinemas = {'gc_1':None, 'gc_2':None, 'gc_3':None}
	seat_counts = {'gc_1':32, 'gc_2':20, 'gc_3':18}	
	gc_screen=Screen(160,90)
	screens = {'gc_1':gc_screen,'gc_2':gc_screen, 'gc_3':gc_screen}

	create_cinemas(cinemas,seat_counts,screens)
	print(cinemas)

def create_cinemas(cinemas,seat_counts,screens):
	'''Creates the Cinema objects for the site'''
	for key in cinemas:
		cinemas[key] = Cinema(capacity=seat_counts[key],screen=screens[key])
		print(cinemas[key].to_string())

def assign_concept(cinemas,concept):
	'''Adds concepts to a collection of Cinema objects'''
	for cinema in cinemas:
		cinema.add_concept(concept)

def main():
	create_site()

if __name__=='__main__':
	main()
