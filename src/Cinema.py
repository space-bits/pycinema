class Cinema:
  def __init__(self, capacity, screen=None, concept=None):
    self.screen = screen
    self.concept = concept 
    self.capacity = capacity

  def set_concept(self, concept):
    self.concept = concept

  def set_screen(self, screen):
    self.screen = screen

  def to_string(self):
    return 'Screen: %s\nConcept: %s\nCapacity: %d' % \
      (self.screen.to_string(), \
      self.concept, self.capacity)
