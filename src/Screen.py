class Screen:
	def __init__(self, width, height):
		self.material = None
		self.width = width
		self.height = height

	def to_string(self):
		return 'width: %d, height: %d' % (self.width, self.height)	
